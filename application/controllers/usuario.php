<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('usuario_model', 'usuarioM');
    }

    public function index() {
        $this->load->view('telaCadastro');
    }

    public function salvar() {
        $dados['nome'] = $this->input->post('nome');
        $dados['email'] = $this->input->post('email');
        $dados['data_nascimento'] = $this->input->post('data_nascimento');
        $dados['senha'] = md5($this->input->post('senha'));

        $query = $this->usuarioM->getEmail($this->input->post('email'));

        if ($query == NULL) {
            $this->usuarioM->addUsuario($dados);

            $this->load->view('msg_sucess');
            $this->load->view('telaCadastro');
        } else {
            $this->load->view('msg_erro_email');
            $this->load->view('telaCadastro');
        }
    }

    public function logar() {
        echo "entrei";
        $email = $this->input->post('email');          
        $senha = md5($this->input->post('senha'));
        
        $query = $this->usuarioM->loginUsuario($email, $senha);

        if ($query != NULL) {
            $dados['usuario'] = $query;
            $dados['logado'] = true;
            $this->session->set_userdata($dados);
            redirect('jogo');
        } else {
            redirect('inicio');
        }
    }
    
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('inicio');
    }

}

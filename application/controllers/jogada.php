<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jogada extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('desafio_model', 'desafioM');
        
    }

    public function inicio($id_desafio = NULL) {
        if ($id_desafio != NULL):
            $query = $this->desafioM->getDesafioByID($id_desafio);
            $dados['desafio'] = $query;
            $this->load->view('cabecalhoJogo');
            $this->load->view('informacao', $dados);
            $this->load->view('rodapeJogo');
        endif;
    }

   

    public function jogar() {
        $this->load->view('cabecalhoJogo');
        $this->load->view('jogoLampada');
        $this->load->view('rodapeJogo');
    }

}

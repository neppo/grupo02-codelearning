<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jogo extends CI_Controller{
    
     public function __construct() {
        parent::__construct();
     
        
        $this->load->model('desafio_model','desafioM');
    }
    
    public function verificarSessao()
    {
        if($this->session->userdata('logado') == false):
            redirect('inicio');
        endif;
    }
    
    public function index()
    {
        $this->verificarSessao();//verifica se a sessão esta aberta para acessar a tela do jogo
        $dados['desafio'] = $this->desafioM->getDesafios();
        $this->load->view('cabecalhoJogo');
        $this->load->view('telaJogo',$dados);
        $this->load->view('rodapeJogo');
    }
    
    
}


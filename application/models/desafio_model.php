<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//impedi o acesso a essa pagina fora do servidor

class desafio_model extends CI_Model{
    
   

    public function addDesafio($dados=NULL)
    {
        if($dados != NULL):
            $this->db->insert('desafio', $dados);
        endif;
    }
    
    public function  getDesafios()
    {
        $query = $this->db->get('desafio');
        
        return $query->result();
    }
    
    public function getDesafioByID($id=NULL)
    {
        if($id != NULL):
            $this->db->where('id_des',$id);
            $this->db->limit(1);
            $query = $this->db->get('desafio');
            return $query->row();
        endif;
    }
        
    }
    
    
    



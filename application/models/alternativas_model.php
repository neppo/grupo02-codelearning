<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class alternativas_model extends CI_Model{
    
    public function getAlternativas(){
        $query = $this->db->get('alternativas');
        return $query->result();
    }
    
    public function addAlternativa($dados = NULL)
    {
        if($dados != NULL):
            $this->db->insert('alternativas',$dados);
        
        endif;
    }
    
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class historico_model extends CI_Model{
    
    public function getHistorico(){
        $query = $this->db->get('historico');
        return $query->result();
    }
    
    public function addHistorico($dados=NULL)
    {
        if($dados !=  NULL):
            $this->db->insert('historico',$dados);
        endif;
    }
    
}

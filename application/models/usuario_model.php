<?php

 defined('BASEPATH') OR exit('No direct script access allowed');
    //impedi o acesso a essa pagina fora do servidor
 
 class usuario_model extends CI_Model{
     
     //função que recebe o email e a senha e busca no banco
     public function loginUsuario($email=NULL, $senha=NULL){
        if($email != NULL && $senha != NULL):
            $this->db->where('email',$email);
            $this->db->where('senha',$senha);
            
            //limita para apenas um registor
            $this->db->limit(1);
            
            //pega o usuario
            $query = $this->db->get('usuario');
            
            return $query->row();
        endif;
     }
    
     
     public function addUsuario($dados=NULL)
     {
         if($dados != NULL):
             $this->db->insert('usuario',$dados);
         endif;
     }
     
     public function getEmail($email=NULL)
     {
         if($email != NULL):
             $this->db->where('email',$email);
             $this->db->limit(1);
             $query = $this->db->get('usuario');
             return $query->row();
             
         endif;
             
         
     }
 }

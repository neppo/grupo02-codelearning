<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
              crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="<?php echo base_url(); ?>estilo/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

    <body>


        <a href="<?php echo base_url(); ?>inicio" class="btn btn-info btn-group fa fa-arrow-circle-left">Voltar</a>

        <form class="form-horizontal" id="formCad" method="post" action="<?php echo base_url(); ?>usuario/salvar" >

            <h1> Cadastro: </h1>
            <div class="container">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="Nome">Nome Completo:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nome" name="nome"placeholder="Informe o seu nome" required="">
                    </div>
                    <span id="spanNome">Inválido!!!</span>
                    <br/>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="Nome" >Data de Nascimento:</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="data"name="data_nascimento" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email:</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" placeholder="informe o seu email" name="email" required="">
                    </div>
                    <span id="spanEmail">Inválido!!!</span>
                    <br/>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pwd">Password:</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="pwd" placeholder="informe a sua senha" name="senha" required="">
                        <span id="spanSenha">Inválido!!!</span>
                        <br/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default ">Enviar</button>
                    </div>
                </div>
        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>bootstrap/js/jquery.mask.js"></script>
    <script src="<?php echo base_url(); ?>bootstrap/js/valida.js"></script>




</body>



</html>
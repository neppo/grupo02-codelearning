<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Jogo Educativo</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
              crossorigin="anonymous">
        <title>Code 4 Learning</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="<?php echo base_url();?>estilo/jogoStyle.css">

    </head>
    <body>
        <nav class="nav">
            <a class="navbar-brand" href="#">CODE LEARNING</a>
            <a class="nav-link active" href="#">RANK</a>
            <a class="nav-link" href="#">TOKENS</a>
            <a class="nav-link navbar-btn" href="<?php echo base_url(); ?>usuario/logout">SAIR</a>
        </nav> 

        <div class="container">
            <div class="card ">
                <div class="card-header bg-info text-white">Jogo Code 4 Learning</div>
                <div class="card-body">
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
              crossorigin="anonymous">
        <title>Code 4 Learning</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="<?php echo base_url(); ?>estilo/style.css">


    </head>

    <body>

        <div class="jumbotron">
            <div class="title">
                <h1> Code 4 Learning</h1>
              
                <p>Aprenda conceitos de Programação de forma descontraída</p>
            </div>

        </div>
        <div class="container ">
            <div class="row">
                <div class="col-sm-8">
                    <p>Venha conhecer o Code 4 Learning, uma plataforma onde você pode aprender aprender conceitos de programação
                        em forma de resolução de problemas e se divertindo no processo.</p>
                    <p>Nossa interface é bem interativa com a forma de mini-jogos onde você pode conseguir pontos para competir
                        com outros usuarios e adquirir Tokens, que é nossa moeda interna, para transações internar e muito mais!</p>
                    <p>Cadastre-se e Confira</p>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">

                        <form id="formLogin" name="formLogin" action="<?php echo base_url(); ?>usuario/logar" method="post" >
                            <p> Login: </p>

                            <label for="usr">E-mail:</label>
                            <input type="text" class="form-control" id="usr" name="email" required="">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control" id="pwd" name="senha" required="">
                            </div>
                    <button type="submit" class="btn btn">Login</button>
                    <a href="<?php echo base_url();?>usuario" class="nounderline">Cadastre-se</a>
                        </form>

                    </div>
                </div>



            </div>

            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>


    </body>

</html>
<!DOCTYPE HTML>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
              crossorigin="anonymous">



        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="<?php echo base_url(); ?>bootstrap/js/jogo.js"></script>

    </head>
    <body>
        <div class="container" id="cont">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <button id="btn3" class="btn-group-justified btn btn-primary" draggable="true" ondragstart="drag(event)" style=" margin-bottom: 20px; ">Buscar uma lampada</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <button id="btn1" class="btn-group-justified btn btn-primary" draggable="true" ondragstart="drag(event)" style=" margin-bottom: 20px;">Pegar uma escada</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <button id="btn2" class="btn-group-justified btn btn-primary" draggable="true" ondragstart="drag(event)" style=" margin-bottom: 20px;">Posicionar a escada embaixo da lampada</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <button id="btn4" class="btn-group-justified btn btn-primary" draggable="true" ondragstart="drag(event)" style=" margin-bottom: 20px;">Subir na escada</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <button id="btn5" class="btn-group-justified btn btn-primary" draggable="true" ondragstart="drag(event)" style=" margin-bottom: 20px;">Retirar a lampada velha</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <button id="btn7" class="btn-group-justified btn btn-primary" draggable="true" ondragstart="drag(event)" style=" margin-bottom: 20px;">Descer da escada</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <button id="btn6" class="btn-group-justified btn btn-primary" draggable="true" ondragstart="drag(event)" style=" margin-bottom: 20px;">Colocar a lampada nova</button>
                        </div>
                    </div>
                </div>
                <div class="col" id="div1" ondrop="drop(event)" ondragover="allowDrop(event)">
                </div>


            </div>
            <button class="btn btn btn-success" id="confirmar" onclick="confirma_evento();">Confirmar</button>
        </div>



    </body>
</html>
 $("#formCad").submit(function () {

            var nome = $("#formCad input:eq(0)").val();
            var data = $("#formCad input:eq(1)").val();
            var email = $("#formCad input:eq(2)").val();
            var senha = $("#formCad input:eq(3)").val();

            validacaoNome(nome);
            validacaoSenha(senha);
        }


        );

        $("#nome").mask(
            "SSSW",
            {
                translation: {
                    W: { pattern: /[\w\s]/, recursive: true }
                },
                onKeyPress: function (value, event) {
                    event.currentTarget.value = value.toUpperCase();
                }
            }
        );

        $("#pwd").mask(
            "AAAAAADDDDDD",
            {
                translation: {
                    D: { pattern: /[a-z0-9]/, optional: true }
                }
            }
        );

        $("span").css("display", "none");

        function validacaoNome(nome) {
            var expRegNome = /[A-Z]{3,}[\s][A-Z]+/;

            if (!expRegNome.test(nome)) {
                $("#spanNome").fadeIn(
                    1500,
                    function () {
                        $("span").css({ "display": "inline", "color": "red" });
                    }
                );

                return false;
            } else {
                $("#spanNome").fadeOut(
                    1500,
                    function () {
                        $("span").css("display", "none");
                    }
                );

                return true;
            }
        }

        function validacaoSenha(senha) {
            var expRegSenha = /[a-z0-9]{6,12}/;

            if (!expRegSenha.test(senha)) {
                $("#spanSenha").fadeIn(
                    1500,
                    function () {
                        $("#spanSenha").css({ "display": "inline", "color": "red" });
                    }
                );

                return false;
            } else {
                $("#spanSenha").fadeOut(
                    1500,
                    function () {
                        $("#spanSenha").css("display", "none");
                    }
                );

                return true;
            }
        }
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    
    // Criar uma div row e uma div col e colocar o elemento dentro.
    let button = document.getElementById(data);
    let div = button.parentElement.parentElement;
    ev.target.appendChild(div);
    
    
}

function  confirma_evento(){
   var id1 = $('#div1 button ').eq(0).attr('id');
   var id2 = $('#div1 button ').eq(1).attr('id');
   var id3 = $('#div1 button ').eq(2).attr('id');
   var id4 = $('#div1 button ').eq(3).attr('id');
   var id5 = $('#div1 button ').eq(4).attr('id');
   var id6 = $('#div1 button ').eq(5).attr('id');
   var id7 = $('#div1 button ').eq(6).attr('id');
   
   if(id1 === 'btn1' 
     && id2 === 'btn2'
     && id3 === 'btn3'
     && id4 === 'btn4'
     && id5 === 'btn5'
     && id6 === 'btn6'
     && id7 === 'btn7'){
     alert('Sequencia de passos correta!');
   }
   else{
       alert('Sequencia de passos errada, tente novamente!');
   }
   location.reload();
   
}
